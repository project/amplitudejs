# AmplitudeJS

The module defines the AmplitudeJS (https://github.com/521dimensions/amplitudejs) audio player external library for Drupal along with media field formatters. These are based on the AmplitudeJS example players:

*   [Blue Playlist](https://521dimensions.com/open-source/amplitudejs/docs/examples/blue-playlist.html)
*   [Flat Black Playlist](https://521dimensions.com/open-source/amplitudejs/docs/examples/flat-black.html)
*   [Single Song Player](https://521dimensions.com/open-source/amplitudejs/docs/examples/single-song.html)
*   [Visualization Player](https://521dimensions.com/open-source/amplitudejs/docs/examples/visualization-player.html)
*   [White Playlist](https://521dimensions.com/open-source/amplitudejs/docs/examples/dynamic-playlist-player.html)
*   [Multiple Songs](https://521dimensions.com/open-source/amplitudejs/docs/examples/multiple-songs.html)
*   [Simple Black Playlist (at the bottom on the page preview)](https://521dimensions.com/open-source/amplitudejs/docs/examples/multiple-playlists.html)

## Installation
------------

1.  Install the module (more information about that here: https://www.drupal.org/docs/extending-drupal/installing-modules).
2.  You must also install the AmplitudeJS library. The module needs the following file: `/libraries/amplitudejs/dist/amplitude.min.js`
    You can get it with composer if your composer.json is set properly for downloading npm packages (more information about that here https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries): `composer require npm-asset/amplitudejs` or you can download it manually from the [https://cdn.jsdelivr.net/npm/amplitudejs@5.3.2/dist/amplitude.min.js](https://cdn.jsdelivr.net/npm/amplitudejs@5.3.2/dist/amplitude.min.js) URL, and copy it to the /libraries/amplitudejs/dist/ folder.
3.  Enable the _amplitudejs_ module - it just defines the external library for Drupal. If you develop a custom formatter based on this library, you'll need just this one.
4.  Enable the _amplitudejs\_formatters_ submodule if you want to use the AmplitudeJS example players as media field formatters.

## How to set the formatters from the amplitudejs\_formatters submodule with media field
--------------------------------------------------------------------

1.  After enabling the module, you'll need to have a media type that will contain the necessary fields for the audio player formatters. If you installed the core media module, there should be an audio media type, you can extend that. The following fields are required in the media type:

    *   A file field that'll contain 1 mp3 file.

    The following fields are optional in the media type:
    *   Text field for song name.
    *   Text field for song artist.
    *   Text field for song album name.
    *   Image field for song album art. (It is not required but advised to create a custom image style that will be used for displaying the album art)
2.  If the media type is done, create a media reference field for your content type.
3.  Set the previously created media type as reference type there.
4.  Navigate to Manage display and set one of the audio player formatters available for that media field.
5.  Open the formatter settings.
6.  There you'll need to assign different fields for different data with tokens. If you installed the contrib token module (https://www.drupal.org/project/token), you can browse the available tokens. Here's a help for setting the tokens:

    *   Audio file field name: provide the file URL, so use **\[media:_audio\_file\_fieldname:entity:url\]** and replace _audio\_file\_fieldname_ with the field's machine name that contains the audio file.
    *   Title field name: use **\[media:name\]** if you want to use the media's own title or if you have a separate field for the song name, use the following: **\[media:_song\_name\_fieldname\]**
    *   Artist field name: **\[media:_artist\_name\_fieldname\]**
    *   Album Art field name: use **\[media:_image\_file\_fieldname_:entity:url\]** for the original image URL. If you want to use an image style instead, use the following: **\[media:_image\_file\_fieldname_:_image\_style\_machine\_name_:url\]** Don't forget to replace the _image\_file\_fieldname_ with the field machine name that contains the image file & replace the _image\_style\_machine\_name_ with the image style's own machine name.
    *   Album field name: **\[media:_album\_field\_name\]**

    Here's an example setting: ![AmplitudeJS formatter token settings example](https://www.drupal.org/files/amplitudejs_formatter_token_example.png)
7.  Save the settings, and now you can create content.

You can set up multiple media fields with multiple audio player formatters but due to the limitations of the AmplitudeJS library, only one player will play the audio simultaneously.

## How to set the formatters from the amplitudejs\_formatters submodule with file field
--------------------------------------------------------------------
- Create file field for your content type. Set to enable the mp3 extension.
- Go to Manage display and set one of the AmplitudeJS player formatters.
- Open the formatter settings.
    *   Audio file field name: provide the file URL, so use **\[file:url\]** .
    *   Title field name: youcan use the file's name with **\[file:name\]** or you can also use another text field from the parent node. Use the "Browse available tokens" button to insert the proper field token. For example for node title: **\[node:title\]**
    *   Artist field name: leave empty or use some field from the parent node.
    *   Album Art field name: leave empty or use some field from the parent node. In the token you must specify an image URL.
    *   Album field name: leave empty or use some field from the parent node.
