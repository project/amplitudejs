<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'amplitudejs_visualization_player'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_visualization_player",
 *   label = @Translation("Visualization Player"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class VisualizationPlayer extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-visualization-player-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_visualization_player';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_visualization_player'];
  }

  protected function getVisualizations() {
    return [
      'visualization' => 'michaelbromley_visualization',
      'visualizations' => ["MichaelBromleyVisualization"],
    ];
  }

}
