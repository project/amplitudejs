<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_blue_playlist'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_blue_playlist",
 *   label = @Translation("Blue Playlist"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class BluePlaylist extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-blue-playlist-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_blue_playlist';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_blue_playlist'];
  }

}
