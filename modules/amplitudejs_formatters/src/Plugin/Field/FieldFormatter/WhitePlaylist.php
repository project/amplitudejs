<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_white_playlist'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_white_playlist",
 *   label = @Translation("White Playlist"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class WhitePlaylist extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-white-playlist-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_white_playlist';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_white_playlist'];
  }

}
