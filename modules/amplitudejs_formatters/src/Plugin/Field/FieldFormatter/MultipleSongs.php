<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_multiple_songs'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_multiple_songs",
 *   label = @Translation("Multiple Songs"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class MultipleSongs extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-multiple-songs-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_multiple_songs';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_multiple_songs'];
  }

  protected function processAlbumArtUrl($media, $albumArtUrl) {
    if ($albumArtUrl === '') {
      $albumArtUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
    }

    return $albumArtUrl;
  }

}
