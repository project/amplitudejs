<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for AmplitudeJS field formatter plugins.
 *
 * @package Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter
 */
abstract class PlayerBase extends EntityReferenceFormatterBase {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\user\Plugin\views\argument_default\CurrentUser
   */
  protected $currentUser;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Image Style storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, $moduleHandler, $entityTypeManager, $currentUser, $token) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'audio_field' => '[media:field_media_audio_file]',
      'title_field' => '[media:name]',
      'artist_field' => '',
      'album_art_field' => '',
      'album_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    if ($this->moduleHandler->moduleExists('token')) {
      $element['token_caption'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Replacement patterns'),
        '#theme' => 'token_tree_link',
        '#token_types' => [$this->fieldDefinition->getFieldStorageDefinition()->getSetting('target_type'), $this->fieldDefinition->getTargetEntityTypeId()],
      ];
    }

    $element['audio_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Audio file field name'),
      '#default_value' => $this->getSetting('audio_field'),
      '#required' => FALSE,
    ];

    $element['title_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title field name'),
      '#default_value' => $this->getSetting('title_field'),
      '#required' => FALSE,
    ];

    $element['artist_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Artist field name'),
      '#default_value' => $this->getSetting('artist_field'),
      '#required' => FALSE,
    ];

    $element['album_art_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Album Art field name'),
      '#default_value' => $this->getSetting('album_art_field'),
      '#required' => FALSE,
    ];

    $element['album_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Album field name'),
      '#default_value' => $this->getSetting('album_field'),
      '#required' => FALSE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Audio file field name: @name', ['@name' => $this->getSetting('audio_field')]);
    $summary[] = $this->t('Title file field name: @name', ['@name' => $this->getSetting('title_field')]);
    $summary[] = $this->t('Artist file field name: @name', ['@name' => $this->getSetting('artist_field')]);
    $summary[] = $this->t('Album Art field name: @name', ['@name' => $this->getSetting('album_art_field')]);
    $summary[] = $this->t('Album field name: @name', ['@name' => $this->getSetting('album_field')]);

    return $summary;
  }

  protected function replaceTokenWithValues($entity, $token, $langcode, &$cacheMetaData, $parentEntity) {
    if (empty($entity)) {
      return NULL;
    }

    return $this->token->replace($token, [$this->fieldDefinition->getFieldStorageDefinition()->getSetting('target_type') => $entity, $this->fieldDefinition->getTargetEntityTypeId() => $parentEntity], [
      'clear' => TRUE,
      'langcode' => $langcode,
      'callback' => 'Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter\PlayerBase::editTitleToken',
    ], $cacheMetaData);
  }

  /**
   * Replace problematic characters in the tokens array.
   *
   * Uses the callback functionality of token to replace HTML encoded characters
   * to readable ones.
   *
   * @param array &$tokens
   *   A array of tokens to modify.
   */
  public static function editTitleToken(array &$tokens) {
    foreach ($tokens as $name => &$token) {
      $value = (string) $token;
      $token = Xss::filter(Html::decodeEntities($value));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entities = $this->getEntitiesToView($items, $langcode);
    $parentEntity = $items->getEntity();
    $cacheMetaData = new BubbleableMetadata();
    $cacheMetaData = $cacheMetaData->addCacheableDependency($parentEntity);

    $songs = [];
    $visualizationResult = $this->getVisualizations();

    if (!empty($entities)) {
      $audioFileNameToken = $this->getSetting('audio_field');
      $songNameToken = $this->getSetting('title_field');
      $songArtistToken = $this->getSetting('artist_field');
      $albumArtToken = $this->getSetting('album_art_field');
      $songAlbumToken = $this->getSetting('album_field');

      foreach ($entities as $delta => $entity) {
        $cacheMetaData = $cacheMetaData->addCacheableDependency($entity);
        $audioFileUrl = $this->replaceTokenWithValues($entity, $audioFileNameToken, $langcode, $cacheMetaData, $parentEntity);
        $songName = $this->replaceTokenWithValues($entity, $songNameToken, $langcode, $cacheMetaData, $parentEntity);
        $songArtist = $this->replaceTokenWithValues($entity, $songArtistToken, $langcode, $cacheMetaData, $parentEntity);
        $songAlbum = $this->replaceTokenWithValues($entity, $songAlbumToken, $langcode, $cacheMetaData, $parentEntity);
        $albumArtUrl = $this->replaceTokenWithValues($entity, $albumArtToken, $langcode, $cacheMetaData, $parentEntity);

        if ($audioFileUrl === '') {
          continue;
        }

        $albumArtUrl = $this->processAlbumArtUrl($entity, $albumArtUrl);

        $song = new \stdClass();
        $song->name = $songName;
        $song->artist = $songArtist;
        $song->album = $songAlbum;
        $song->url = $audioFileUrl;
        $song->cover_art_url = $albumArtUrl;

        if (!empty($visualizationResult)) {
          $song->visualization = $visualizationResult['visualization'];
        }

        $songs[] = $song;
      }

    }

    $id = $this->getHtmlId();

    $moduleRelativePath = \Drupal::service('extension.list.module')->getPath('amplitudejs_formatters');
    $theme = $this->getTheme();

    $renderArray = [
      '#theme' => $theme,
      '#has_songs' => !empty($songs),
      '#html_id' => $id,
      '#songs' => $songs,
      '#module_path' => $moduleRelativePath,
    ];

    $cacheMetaData->applyTo($renderArray);

    $renderArray['#attached']['drupalSettings']['amplitudejs_formatters'][$id]['songsObject'] = $songs;
    $renderArray['#attached']['drupalSettings']['amplitudejs_formatters'][$id]['addClickHandler'] = $this->isAddJsClickHandlerToPlayer();
    $renderArray['#attached']['drupalSettings']['amplitudejs_formatters'][$id]['theme'] = $theme;

    $renderArray['#attached']['drupalSettings']['amplitudejs_formatters'][$id]['visualization'] =
      $visualizationResult['visualization'] ?? NULL;

    $renderArray['#attached']['drupalSettings']['amplitudejs_formatters'][$id]['visualizations'] =
      $visualizationResult['visualizations'] ?? NULL;

    $renderArray['#attached']['library'] = $this->getAttachedLibraries();

    return [$renderArray];
  }

  protected abstract function getHtmlId();

  protected abstract function getTheme();

  protected abstract function isAddJsClickHandlerToPlayer();

  protected abstract function getAttachedLibraries();

  protected function processAlbumArtUrl($media, $albumArtUrl) {
    return $albumArtUrl;
  }

  protected function getVisualizations() {
    return NULL;
  }
}
