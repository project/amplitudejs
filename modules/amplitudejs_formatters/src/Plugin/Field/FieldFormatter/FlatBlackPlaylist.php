<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_flat_black_playlist'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_flat_black_playlist",
 *   label = @Translation("Flat Black Playlist"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class FlatBlackPlaylist extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-flat-black-playlist-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_flat_black_playlist';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_flat_black_playlist'];
  }

}
