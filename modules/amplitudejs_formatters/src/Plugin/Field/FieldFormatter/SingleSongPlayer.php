<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_single_song_player'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_single_song_player",
 *   label = @Translation("Single Song Player"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class SingleSongPlayer extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-single-song-player-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_single_song_player';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_single_song_player'];
  }

}
