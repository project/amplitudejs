<?php

namespace Drupal\amplitudejs_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Crypt;

/**
 * Plugin implementation of the 'amplitudejs_simple_black_playlist'.
 *
 * @FieldFormatter(
 *   id = "amplitudejs_simple_black_playlist",
 *   label = @Translation("Simple Black Playlist"),
 *   field_types = {
 *     "entity_reference",
 *     "file"
 *   }
 * )
 */
class SimpleBlackPlaylist extends PlayerBase {

  /**
   * {@inheritdoc}
   */
  protected function getHtmlId() {
    return 'amplitudejs-simple-black-playlist-' . Crypt::randomBytesBase64(8);
  }

  /**
   * {@inheritdoc}
   */
  protected function getTheme() {
    return 'amplitudejs_simple_black_playlist';
  }

  /**
   * {@inheritdoc}
   */
  protected function isAddJsClickHandlerToPlayer() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAttachedLibraries() {
    return ['amplitudejs_formatters/amplitudejs_simple_black_playlist'];
  }

  protected function processAlbumArtUrl($media, $albumArtUrl) {
    if ($albumArtUrl === '') {
      $albumArtUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
    }

    return $albumArtUrl;
  }

}
