(function (Drupal, drupalSettings) {

  'use strict';

  let initializedPlayers = [];
  let isEveryPlayerInitialized = false;

  Drupal.behaviors.amplitudejs_formatters = {
    attach: function (context, settings) {
      if (!isEveryPlayerInitialized) {

        if (!isset(drupalSettings.amplitudejs_formatters)) {
          return;
        }

        let result = initAmplitudeOnLoad();

        if (result) {
          isEveryPlayerInitialized = true;
        }
      }
    }
  };

  function initAmplitudeOnLoad() {
    for (let playerId in drupalSettings.amplitudejs_formatters) {

      if (initializedPlayers.indexOf(playerId) > -1) {
        continue;
      }

      let isInitSuccessful = initPlayer(drupalSettings.amplitudejs_formatters[playerId], playerId);

      if (isInitSuccessful) {
        initializedPlayers.push(playerId);
      }
    }

    return isEveyPlayerLoaded();
  }

  function isEveyPlayerLoaded() {
    return (initializedPlayers.length === Object.keys(drupalSettings.amplitudejs_formatters).length);
  }

  function initPlayer(data, playerId) {
    let songs = data['songsObject'];
    let visualization = data['visualization'];
    let visualizations = data['visualizations'];

    if (empty(songs)) {
      return true;
    }

    let player = document.querySelector("#" + playerId);
    if (empty(player)) {
      return false;
    }

    let isInitialized = Amplitude.getConfig().playlists;

    // Init Amplitude if it hasn't been initialized.
    if (empty(isInitialized)) {
      let userConfig = {
        "songs": songs,
        "playlists": {
          [playerId]: {
            "songs": songs
          }
        },
        "waveforms": {
          "sample_rate": 50
        },
        "volume": 100
      };

      if (isset(visualization) && isset(visualizations)) {
        userConfig.visualization = visualization;
        let vis = [];
        for (let i = 0; i < visualizations.length; i++) {
          vis.push({
            object: window[visualizations[i]],
            params: {}
          })
        }
        userConfig.visualizations = vis;
      }

      Amplitude.init(userConfig);

      // Bugfix: need to call the pause function on load,
      // otherwise Chrome mobile doesn't output the time the the during playback.
      Amplitude.pause();
    }
    else {
      let playlistMetadata = {};

      Amplitude.addPlaylist(playerId, playlistMetadata, songs);
      Amplitude.bindNewElements();
    }

    player.classList.add("js-initialized");

    // Space pauses the player.
    // But only when the active element is not a textbox.
    window.onkeydown = function (e) {
      let clickEvent = new MouseEvent("click", {
          "view": window,
          "bubbles": true,
          "cancelable": false
      });

      let dontDisableKeysInElements = [
        'input',
        'INPUT',
        'textarea',
        'TEXTAREA',
      ];
      if (dontDisableKeysInElements.indexOf(document.activeElement.tagName) === -1) {
        if (isset(e.code)) {
          let isSpace = (e.code === "Space");
          let activePlayer = getActivePlayer();
          if (isSpace && isset(activePlayer)) {
            document.querySelector("#" + activePlayer + "-play-pause").dispatchEvent(clickEvent);
          }
          return !isSpace;
        }
        if (isset(e.keyCode)) {
          let isSpace = (e.keyCode === 32);
          let activePlayer = getActivePlayer();
          if (isSpace && isset(activePlayer)) {
            document.querySelector("#" + activePlayer + "-play-pause").dispatchEvent(clickEvent);
          }
          return !isSpace;
        }
      }

    };

    initExtraJsEvents(data, playerId);

    return true;
  }

  function isset(value) {
    return !(value === null || typeof value === "undefined");
  }

  function empty(value) {
    let isValueSet = isset(value);
    return !isValueSet
      || (isValueSet && value == 0)
      || (isValueSet && typeof value === "object" && value.length === 0)
      || (isValueSet && typeof value === "object" && Object.keys(value).length === 0 && value.constructor === Object)
      || (isValueSet && typeof value === "string" && value.length === 0);
  }

  function getActivePlayer() {
    let active = Amplitude.getActivePlaylist();

    if (!empty(active)) {
      return active;
    }

    // Get the closest initialized player to the top if there's no active.
    let initialized = document.querySelector(".amplitude-player-wrapper.js-initialized");

    if (isset(initialized)) {
      return initialized.id;
    }

    return null;
  }

  function initExtraJsEvents(data, playerId) {
    let addClickHandler = data["addClickHandler"];

    if (!empty(addClickHandler)) {
      // Handles a click on the song played progress bar.
      document.querySelector("#song-played-progress-" + playerId).addEventListener('click', function (e) {
        let offset = this.getBoundingClientRect();
        let x = e.pageX - offset.left;
        Amplitude.setSongPlayedPercentage( ( parseFloat(x) / parseFloat(this.offsetWidth) ) * 100 );
      });
    }

    let theme = data["theme"];

    switch (theme) {
      case "amplitudejs_blue_playlist":
        initExtraJsEventsForBluePlaylist(data, playerId);
        break;

      case "amplitudejs_flat_black_playlist":
        initExtraJsEventsForFlatBlackPlaylist(data, playerId);
        break;

      case "amplitudejs_white_playlist":
        initExtraJsEventsForWhitePlaylist(data, playerId);
        break;

      case "amplitudejs_visualization_player":
        initExtraJsEventsForVisualizationPlayer(data, playerId);
        break;
    }

  }

  function initExtraJsEventsForBluePlaylist(data, playerId) {
    let songElements = document.querySelectorAll("#" + playerId + " .amplitude-right .song");

    for(let i = 0; i < songElements.length; i++){
      /*
        Ensure that on mouseover, CSS styles don't get messed up for active songs.
      */
      songElements[i].addEventListener('mouseover', function () {
        this.style.backgroundColor = '#00A0FF';

        this.querySelectorAll('.song-meta-data .song-title')[0].style.color = '#FFFFFF';
        this.querySelectorAll('.song-meta-data .song-artist')[0].style.color = '#FFFFFF';
        this.querySelectorAll('.song-duration')[0].style.color = '#FFFFFF';

        if( !this.classList.contains('amplitude-active-song-container') ){
          this.querySelectorAll('.play-button-container')[0].style.display = 'block';
        }

      });

      /*
        Ensure that on mouseout, CSS styles don't get messed up for active songs.
      */
      songElements[i].addEventListener('mouseout', function () {
        this.style.backgroundColor = '#FFFFFF';
        this.querySelectorAll('.song-meta-data .song-title')[0].style.color = '#272726';
        this.querySelectorAll('.song-meta-data .song-artist')[0].style.color = '#607D8B';
        this.querySelectorAll('.play-button-container')[0].style.display = 'none';
        this.querySelectorAll('.song-duration')[0].style.color = '#607D8B';
      });

      /*
        Show and hide the play button container on the song when the song is clicked.
      */
      songElements[i].addEventListener('click', function () {
        this.querySelectorAll('.play-button-container')[0].style.display = 'none';
      });
    }
  }

  function initExtraJsEventsForFlatBlackPlaylist(data, playerId) {
    /*
      Handles a click on the down button to slide down the playlist.
    */
    let headerElements = document.querySelectorAll("#" + playerId + " .down-header");

    for (let i = 0; i < headerElements.length; i++) {
      headerElements[i].addEventListener('click', function () {
        let list = document.querySelector("#" + playerId + " .list");

        list.style.height = "367px";

        document.querySelector("#" + playerId + " .list-screen").classList.remove('slide-out-top');
        document.querySelector("#" + playerId + " .list-screen").classList.add('slide-in-top');
        document.querySelector("#" + playerId + " .list-screen").style.display = "block";
      });
    }

    /*
      Handles a click on the up arrow to hide the list screen.
    */
    let hidePlaylistElements = document.querySelectorAll("#" + playerId + " .hide-playlist");

    for (let i = 0; i < hidePlaylistElements.length; i++) {
      hidePlaylistElements[i].addEventListener('click', function () {
        document.querySelector("#" + playerId + " .list-screen").classList.remove('slide-in-top');
        document.querySelector("#" + playerId + " .list-screen").classList.add('slide-out-top');
        document.querySelector("#" + playerId + " .list-screen").style.display = "none";
      });
    }
  }

  function initExtraJsEventsForWhitePlaylist(data, playerId) {
    /*
      Shows the playlist
    */
    let showPlaylistButton = document.querySelector("#" + playerId + " .show-playlist");

    if (isset(showPlaylistButton)) {
      showPlaylistButton.addEventListener('click', function () {
        document.querySelector("#" + playerId + " .white-player-playlist-container").classList.remove('slide-out-top');
        document.querySelector("#" + playerId + " .white-player-playlist-container").classList.add('slide-in-top');
        document.querySelector("#" + playerId + " .white-player-playlist-container").style.display = "block";
      });
    }

    /*
      Hides the playlist
    */
    let closePlaylistButton = document.querySelector("#" + playerId + " .close-playlist");

    if (isset(closePlaylistButton)) {
      closePlaylistButton.addEventListener('click', function () {
        document.querySelector("#" + playerId + " .white-player-playlist-container").classList.remove('slide-in-top');
        document.querySelector("#" + playerId + " .white-player-playlist-container").classList.add('slide-out-top');
        document.querySelector("#" + playerId + " .white-player-playlist-container").style.display = "none";
      });
    }

  }

  function initExtraJsEventsForVisualizationPlayer(data, playerId) {
    let visualizationToggle = document.querySelector("#" + playerId + " .visualization-toggle");

    if (isset(visualizationToggle)) {
      visualizationToggle.addEventListener('click', function (){
        if( this.classList.contains( 'visualization-off' ) ){
          this.classList.remove('visualization-off');
          this.classList.add('visualization-on');
          document.querySelector("#" + playerId + " .large-now-playing-album-art").style.display = 'none';
          document.querySelector("#" + playerId + " .large-visualization").style.display = 'block';
        }
        else{
          this.classList.remove('visualization-on');
          this.classList.add('visualization-off');
          document.querySelector("#" + playerId + " .large-now-playing-album-art").style.display = 'block';
          document.querySelector("#" + playerId + " .large-visualization").style.display = 'none';
        }
      });
    }

    let arrowUpIcon = document.querySelector("#" + playerId + " .arrow-up-icon");

    if (isset(arrowUpIcon)) {
      arrowUpIcon.addEventListener('click', function(){
        document.querySelector("#" + playerId + " .visualizations-player-playlist").style.display = 'block';
      });
    }
    
    let arrowDownIcon = document.querySelector("#" + playerId + " .arrow-down-icon");

    if (isset(arrowDownIcon)) {
      arrowDownIcon.addEventListener('click', function(){
        document.querySelector("#" + playerId + " .visualizations-player-playlist").style.display = 'none';
      });
    }

  }

})(Drupal, drupalSettings);
